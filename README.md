#Titanic Survivor

Algorithm that predicts by entering certain data whether a person could survive or not survive the collapse of the titanic

##Installation and Execution Instructions

1. Install jupyter notebook
2. Run jupyter notebook
3. Search for main.ipynb file
4. In line 24 of the jupyter book enter the values to make the prediction in the following order:

    1. Ticket class
    2. Age
    3. of siblings / spouses aboard the Titanic(1=True/0=False)
    4. of parents / children aboard the Titanic
    5. Passenger fare,Sex(1=male/0=female)
    6. Port of Embarkation Q = Queenstown(1=True/0=False), S = Southampton(1=True/0=False)

5. And then in the subsequent line we will obtain the value between 0 and 1 (1=True/0=False)
6. And on line 25 we measure the probability of success